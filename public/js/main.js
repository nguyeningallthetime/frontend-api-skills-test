let fetchRecipe = fetch('http://localhost:3001/recipes/');
let fetchSpecials = fetch('http://localhost:3001/specials');

Promise.all([fetchRecipe, fetchSpecials])
  .then((values) => {
    return Promise.all(values.map((response) => response.json()));
  })
  .then(([recipesData, specialsData]) => {
    init(recipesData, specialsData);
  })
  .catch((err) => console.log(err));

// CREATE CLOSURE AND VARIABLE SCOPE
function init(recipes, specials) {
  const nav = document.createElement('nav');
  const ul = document.createElement('ul');
  const header = document.querySelector('#main-header');
  const main = document.querySelector('main');

  ul.classList.add('recipes-list');
  nav.classList.add('main-nav');
  nav.appendChild(ul);
  header.appendChild(nav);

  let recipesResponse = recipes;
  let specialsResponse = specials;

  listRecipes(recipesResponse);
  detailView(recipesResponse[0]);
  nav.addEventListener('click', showDetail);

  // CREATE LIST VIEW
  function listRecipes(recipes) {
    recipes.map((recipe) => {
      const li = document.createElement('li');
      let { uuid, images, title } = recipe;

      let cardContent = `
        <a class="card-btn" href="#" target="_blank" data-id="${uuid}">
          <div class="card-btn__container">
            <div class="card-btn__image-wrap">
              <img src="${images.small}" alt="${title}">
            </div>
            <div class="card-btn__text-wrap">
              <p>${title}</p>
            </div>
          </div>
        </a>
      `;

      li.innerHTML = cardContent;
      ul.appendChild(li);
    });
  }

  // CREATE CLICK HANDLER
  function showDetail(e) {
    if (e.target.classList.contains('card-btn')) {
      e.preventDefault();

      let recipeID = e.target.dataset.id;
      let clickedRecipe = recipesResponse.find((recipe) => recipe.uuid == recipeID);

      detailView(clickedRecipe);
    }
  }

  // CREATE DETAIL VIEW
  function detailView(recipe) {
    // destructure
    let { title, description, ingredients, images, directions, prepTime, servings } = recipe;

    //From project specifications: Ingredients with a matching ingredientId listed in the specials response should also show the special title, type and text under the ingredient name
    // adding specials properties to the ingredients that have matching ingredient id
    ingredients.forEach((ingredient) => {
      specialsResponse.forEach((special) => {
        if (ingredient.uuid == special.ingredientId) {
          ingredient.type = special.type;
          ingredient.title = special.title;
          ingredient.text = special.text;
        }
      });
    });

    let article = `
      <article class="recipe-details">
          <header class="recipe-details__header">
            <h1>${title}</h1>
            <p class="recipe-details__descriptions">${description}<br> <span>Preparation time: ${prepTime}mins<br>Serves: ${servings}</span></p>
          </header>
          <section class="recipe-details__content">
            <section class="recipe-details__directions">
              <img class="recipe-details__image" src="${images.medium}" alt="${title}" />
              <h2>Directions</h2>
              <ol>
                ${directions.map((direction) => `<li>${direction.instructions}</li>`).join(' ')}
              </ol>            
            </section>
            <section class="recipe-details__ingredients">
              <h2>Ingredients</h2>
              <ul class="recipe-details__ingredients-list">
                ${ingredients
                  .map(
                    (ingredient) =>
                      `<li data-ingredientID="${ingredient.uuid}">${
                        ingredient.amount ? ingredient.amount : ''
                      } ${ingredient.measurement} ${ingredient.name} ${
                        ingredient.type
                          ? `<div class="special"><span class="special__type">${ingredient.type}:</span> <span class="special__title">${ingredient.title}</span> <span class="special__text">${ingredient.text}</span></div>`
                          : ''
                      }</li>`
                  )
                  .join('')}
              </ul>
            </section>
          </section>          
      </article>
    `;

    main.innerHTML = article;
  }
}
